//
//  SideMenuViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 19/09/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import UIKit

extension SideMenuViewController: AlertViewDelegate {
    
    func okAction() {
        
        self.alertView.isHidden = true
    }
    
}

class SideMenuViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SideMenuViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SideMenuViewController
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomTableView: UITableView!
    
    @IBOutlet weak var imgLogo:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblDegree:UILabel!
    
    var expireValue:String?
    let currentDate = Date()
    
    var isLogOut:Bool = false
    
    
    var itemsBottomTable: [String] = ["Terms & Conditions", "Privacy Policy","Faq & Support"]
    var itemsProfile: [String] = ["Home", "My Profile" , "My Listing" , "Settings"  , "Messages" , "Logout" ]
    var imagesProfile: [UIImage] = [UIImage(named:"white_home_icon")!, UIImage(named:"myProfile_icon")!, UIImage(named:"myListing_icon")!, UIImage(named:"settings_icon")!, UIImage(named:"messages_icon")!, UIImage(named:"logout_icon")!]
    
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Assign tags to tableViews
        self.tableView.tag = 0;
        self.bottomTableView.tag = 1;
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        
        //Register Cell
        let cell = UINib(nibName:String(describing:SideMenuTableViewCell.self), bundle: nil)
        self.tableView.register(cell, forCellReuseIdentifier: String(describing: SideMenuTableViewCell.self))
        let bottomMenuCell = UINib(nibName:String(describing:SideMenuBottomTableViewCell.self), bundle: nil)
        self.bottomTableView.register(bottomMenuCell, forCellReuseIdentifier: String(describing: SideMenuBottomTableViewCell.self))
//        self.bottomTableView.register(cell, forCellReuseIdentifier: String(describing: SideMenuTableViewCell.self))
//        NotificationCenter.default.addObserver(self, selector: #selector(self.getProfileUpdate), name: NSNotification.Name(rawValue: USERUPDATED), object: nil)
//
//        if let image = Singleton.sharedInstance.CurrentUser!.profileImage {
//            self.imgLogo.setImageFromUrl(urlStr: image)
//        }
        if Singleton.sharedInstance.CurrentUser?.profileImage != nil {
            let imageUrl = Singleton.sharedInstance.CurrentUser?.profileImage!
            self.imgLogo.setImageFromUrl(urlStr: imageUrl!)
        }
        
        if Singleton.sharedInstance.CurrentUser?.fullName != nil {
            self.lblName.text = Singleton.sharedInstance.CurrentUser?.fullName
        }
//
//        if let firstName = Singleton.sharedInstance.CurrentUser!.firstName {
//            let lastName = Singleton.sharedInstance.CurrentUser!.lastName!
//            self.lblName.text = firstName + " " + lastName
//        }
//
//        if let education = Singleton.sharedInstance.CurrentUser!.educationType {
//            self.lblDegree.text = education
//        }
        
    }
    
    @objc func getProfileUpdate() {
        
//        if let image = Singleton.sharedInstance.CurrentUser!.profilePicture {
//            self.imgLogo.setImageFromUrl(urlStr: image)
//        }
//
//        if let firstName = Singleton.sharedInstance.CurrentUser!.firstName {
//            let lastName = Singleton.sharedInstance.CurrentUser!.lastName!
//            self.lblName.text = firstName + " " + lastName
//        }
//
//        if let education = Singleton.sharedInstance.CurrentUser!.educationType {
//            self.lblDegree.text = education
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 0 {
            return self.itemsProfile.count
        }
        else {
            return self.itemsBottomTable.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 0 {
            return GISTUtility.convertToRatio(55)
        }
        else {
            return GISTUtility.convertToRatio(45)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 0 {
            
            let cell:SideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell", for: indexPath) as! SideMenuTableViewCell
            
            cell.lblTitle.text = self.itemsProfile[indexPath.row]
            cell.imgView.image = self.imagesProfile[indexPath.row]
            
            // Hide line view as there is no need of it
            cell.lineView.isHidden = true
            //        if indexPath.row == 5 {
            //            cell.lineView.isHidden = true
            //        }
            return cell
        }
        else {
            let cell:SideMenuBottomTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuBottomTableViewCell", for:indexPath) as! SideMenuBottomTableViewCell
            cell.lblTitle.text = self.itemsBottomTable[indexPath.row]
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 0 {
            
            if indexPath.row == 0 {
                
                let navigationController = sideMenuController?.rootViewController as! UINavigationController
                let vc = HomeViewController.instantiateFromStoryboard()
                navigationController.setViewControllers([vc], animated: false)
                self.hideLeftViewAnimated(self)
                
                //            let navigationController = sideMenuController?.rootViewController as! UINavigationController
                //            let vc = SignInViewController.instantiateFromStoryboard()
                //            navigationController.setViewControllers([vc],animated: false)
                //            self.hideLeftViewAnimated(self)
                
                
            }
            else if indexPath.row == 1 {
                let navigationController = sideMenuController?.rootViewController as! UINavigationController
                let vc = MyProfileViewController.instantiateFromStoryboard()
                navigationController.setViewControllers([vc],animated: false)
                self.hideLeftViewAnimated(self)
                
            }
            else if indexPath.row == 2 {
                
            }
            else if indexPath.row == 5 {
                
                let navigationController = sideMenuController?.rootViewController as! UINavigationController
                let vc = HomeViewController.instantiateFromStoryboard()
                vc.isLogout = true
                navigationController.setViewControllers([vc], animated: false)
                self.hideLeftViewAnimated(self)
                
//                UserDefaults.standard.removeObject(forKey: User_data_userDefault)
//                UserDefaults.standard.synchronize()
//                Singleton.sharedInstance.CurrentUser = nil
//
//                let nav = RootViewController.instantiateFromStoryboard()
//                AppDelegate.getInstatnce().window?.rootViewController = nav
//                let VC = SignInViewController.instantiateFromStoryboard()
//                nav.pushViewController(VC, animated: true)
                
            }
        }
        else {
            if indexPath.row == 0 {
                let navigationController = sideMenuController?.rootViewController as! UINavigationController
                let vc = TermsAndConditionsViewController.instantiateFromStoryboard()
                navigationController.setViewControllers([vc], animated: false)
                self.hideLeftViewAnimated(self)
            }
            else if indexPath.row == 1 {
                let navigationController = sideMenuController?.rootViewController as! UINavigationController
                let vc = PrivacyPolicyViewController.instantiateFromStoryboard()
                navigationController.setViewControllers([vc], animated: false)
                self.hideLeftViewAnimated(self)
            }
            else if indexPath.row == 2 {
                let navigationController = sideMenuController?.rootViewController as! UINavigationController
                let vc = FAQAndSupportViewController.instantiateFromStoryboard()
                navigationController.setViewControllers([vc], animated: false)
                self.hideLeftViewAnimated(self)
            }
        }
        
    }
    
}




