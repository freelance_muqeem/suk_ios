//
//  RootViewController.swift
//  Merchant
//
//  Created by Abdul Muqeem on 19/09/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import UIKit

class RootViewController: UINavigationController {

    class func instantiateFromStoryboard() -> RootViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RootViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
