//
//  SideMenuRootViewController.swift
//  Merchant
//
//  Created by Abdul Muqeem on 19/09/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import UIKit
import LGSideMenuController

class SideMenuRootViewController: LGSideMenuController {

    class func instantiateFromStoryboard() -> SideMenuRootViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SideMenuRootViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(red:0.00, green:0.45, blue:1.00, alpha:1.0)
        
        self.leftViewController = SideMenuViewController.instantiateFromStoryboard()
        let root = RootViewController.instantiateFromStoryboard()
        let vc = HomeViewController.instantiateFromStoryboard()
        root.viewControllers = [ vc ]
        self.rootViewController = root
    }

}
