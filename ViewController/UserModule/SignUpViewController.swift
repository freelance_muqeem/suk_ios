//
//  SignUpViewController.swift
//  Suk
//
//  Created by AQEEL AHMED on 21/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import MRCountryPicker

extension SignUpViewController : AlertViewDelegate, UITextFieldDelegate, MRCountryPickerDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class SignUpViewController: UIViewController {

    class func instantiateFromStoryboard() -> SignUpViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SignUpViewController
    }
    
    // IBOutlets & IBActions
    @IBOutlet weak var nameTextField: CardTextField!
    @IBOutlet weak var emailTextField: CardTextField!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: CardTextField!
    
    @IBOutlet weak var agreeTermsBtn: UIButton!
    @IBOutlet weak var agreeTermsCheckbox: UIButton!
    
    @IBOutlet weak var signupBtn: UIButton!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    var counter:Int = 0
    var isAgreeToTerms:Bool = false
    
    var countryCodePicker:MRCountryPicker!
//    @IBOutlet weak var countryCodePicker: MRCountryPicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        // MRCountryPicker Setup
        self.countryCodePicker = MRCountryPicker()
        self.countryCodePicker.countryPickerDelegate = self
        self.countryCodePicker.showPhoneNumbers = true
        
        
        
        // Set countryCode TextField Picker
        self.countryCodeTextField.delegate = self;
        self.countryCodeTextField.inputView = self.countryCodePicker
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.HideNavigationBar()
    }
    
    @IBAction func googleBtn(_ sender: Any) {
    }
    @IBAction func faceboolBtn(_ sender: Any) {
    }
    @IBAction func btnTermsCheckboxAction(_ sender: UIButton) {
        self.checkBoxAction()
    }
    
    @IBAction func signupAction(_ sender: Any) {
        
        self.view.endEditing(true)

        guard let name = self.nameTextField.text, AppHelper.isValidText(testStr: name) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid Name" , style: .danger)
            return
        }

        guard let email = self.emailTextField.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid email" , style: .danger)
            return
        }

        guard let countryCode = self.countryCodeTextField.text, AppHelper.isValidText(testStr: countryCode) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid Mobile Country Code" , style: .danger)
            return
        }

        guard let phoneNumber = self.phoneTextField.text, AppHelper.isValidPhone(testStr: phoneNumber) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid Phone Number" , style: .danger)
            return
        }

        guard let password = self.passwordTextField.text, AppHelper.isValidPassword(testStr: password) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid Password" , style: .danger)
            return
        }
        
        if isAgreeToTerms == false {
            self.showBanner(title: "Error", subTitle: "Please Agree to Terms ans Conditions" , style: .danger)
            return
        }

        self.startLoading(message: "")

        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }

        let params:[String:Any] = ["email":email , "full_name":name,  "phone":(countryCode+phoneNumber), "password":password]
        print("Parameter: \(params)")

        UserServices.Register(param: params, completionHandler: {(status, response, error) in

            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }

            self.stopLoading()
            print("Response: \(response!)")

            let userResultObj = User(object:(response?["Result"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()


            let nav = RootViewController.instantiateFromStoryboard()
            AppDelegate.getInstatnce().window?.rootViewController = nav
            let VC = TutorialViewController.instantiateFromStoryboard()
            nav.pushViewController(VC, animated: true)

//            let controller = SideMenuRootViewController.instantiateFromStoryboard()
//            controller.leftViewPresentationStyle = .scaleFromBig
//            controller.leftViewWidth = 290.0
//            AppDelegate.getInstatnce().window?.rootViewController = controller

        })
        
    }
    
    @IBAction func signInAction(_ sender: UIButton) {
        let vc = SignInViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func agreeTermsAction(_ sender: UIButton) {
        let vc = TermsAndConditionsViewController.instantiateFromStoryboard()
        vc.ifFromSignup = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

// MARK:- Helper Functions
extension SignUpViewController {
    func checkBoxAction() {
        self.counter += 1
        if self.counter % 2 != 0 {
            self.agreeTermsCheckbox.setImage(UIImage(named: "ic_checkbox"), for: .normal)
            self.isAgreeToTerms = true
        }
        else if (self.counter % 2 == 0) {
            self.agreeTermsCheckbox.setImage(UIImage(named: "ic_uncheck"), for: .normal)
            self.isAgreeToTerms = false
        }
    }
}

// MARK:- CountryCode Picker
extension SignUpViewController {
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.countryCodeTextField.text = phoneCode
    }
    
    
}
