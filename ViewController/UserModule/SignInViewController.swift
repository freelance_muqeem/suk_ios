//
//  SignInViewController.swift
//  Suk
//
//  Created by AQEEL AHMED on 21/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension SignInViewController : AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class SignInViewController: UIViewController {

    class func instantiateFromStoryboard() -> SignInViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SignInViewController
    }
    
    
    // IBOutlets and Variables
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var btnRememberCheckbox: UIButton!
    
    var counter:Int = 0
    var isRememberMe:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.initData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.HideNavigationBar()
    }
    
    @IBAction func loginAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        if isRememberMe == true {
            self.saveDataInUserDefaults()
        }
        else {
            self.removeDataFromUserDefaults()
        }
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid email" , style: .danger)
            return
        }
        
        guard let password = self.txtPassword.text, AppHelper.isValidPassword(testStr: password) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid password" , style: .danger)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["email":email , "password":password ]
        print("Parameter: \(params)")
        
        UserServices.Login(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print("Response: \(response!)")
            
            let userResultObj = User(object:(response?["Result"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            let controller = SideMenuRootViewController.instantiateFromStoryboard()
            controller.leftViewPresentationStyle = .scaleFromBig
            controller.leftViewWidth = 290.0
            AppDelegate.getInstatnce().window?.rootViewController = controller
            
        })
    }

    @IBAction func forgotPassword(_ sender: UIButton) {
        let vc = ForgotPasswordViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signUpAction(_ sender: UIButton) {
        let vc = SignUpViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnRememberCheckbox(_ sender: UIButton) {
        self.checkBoxAction()
    }
    
}

//MARK:- Helper Functions
extension SignInViewController {
    func initData() {
        let userDefaults = UserDefaults.standard
        if userDefaults.object(forKey: "email") != nil {
            self.txtEmail.text = (userDefaults.object(forKey: "email") as! String)
        }
        if userDefaults.object(forKey: "password") != nil {
            self.txtPassword.text = (userDefaults.object(forKey: "password") as! String)
        }
    }
    
    func checkBoxAction() {
        self.counter += 1
        if self.counter % 2 != 0 {
            self.btnRememberCheckbox.setImage(UIImage(named: "ic_checkbox"), for: .normal)
            isRememberMe = true
        }
        else if (self.counter % 2 == 0) {
            self.btnRememberCheckbox.setImage(UIImage(named: "ic_uncheck"), for: .normal)
            isRememberMe = false
        }
    }
    
    func saveDataInUserDefaults() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(self.txtEmail.text, forKey:"email")
        userDefaults.set(self.txtPassword.text, forKey: "password")
        userDefaults.synchronize()
    }
    func removeDataFromUserDefaults() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "email")
        userDefaults.removeObject(forKey: "password")
        userDefaults.synchronize()
    }
}
