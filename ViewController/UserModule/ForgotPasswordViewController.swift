//
//  ForgotPasswordViewController.swift
//  Suk
//
//  Created by AQEEL AHMED on 20/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension ForgotPasswordViewController : AlertViewDelegate {
    
    func okAction() {
        
        if self.textAlert == "success" {
            self.navigationController?.popViewController(animated: true)
        }
        self.alertView.isHidden = true
    }
    
}

class ForgotPasswordViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ForgotPasswordViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ForgotPasswordViewController
    }
    
    // IBOutlets and Variables
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var txtEmail: CardTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.ShowNavigationBar()
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid email" , style: .danger)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["email":email]
        print("Parameter: \(params)")
        
        UserServices.ForgotPassword(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print("Response: \(response!)")
            
            self.alertView.alertShow(image: SUCCESS_IMAGE, title: "Success", msg: "Your request has been sent successfully", id: 0)
            self.alertView.isHidden = false
            self.textAlert = "success"
            
        })
    }
    
}

