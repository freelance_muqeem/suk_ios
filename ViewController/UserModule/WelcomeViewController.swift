//
//  WelcomeViewController.swift
//  Suk
//
//  Created by AQEEL AHMED on 27/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    class func instantiateFromStoryboard() -> WelcomeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! WelcomeViewController
    }
    
    // IBOutlets and Variables
    @IBOutlet weak var logoImgVw: UIImageView!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnCreateAcc: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.HideNavigationBar()
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
//        let nav = RootViewController.instantiateFromStoryboard()
//        AppDelegate.getInstatnce().window?.rootViewController = nav
        let VC = SignInViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(VC, animated: true)
        //nav.pushViewController(VC, animated: true)
        
    }
    
    @IBAction func createAccAction(_ sender: UIButton) {let nav = RootViewController.instantiateFromStoryboard()
        AppDelegate.getInstatnce().window?.rootViewController = nav
        let VC = SignUpViewController.instantiateFromStoryboard()
        nav.pushViewController(VC, animated: true)
    }
    
}
