//
//  ViewController.swift
//  Suk
//
//  Created by Abdul Muqeem on 09/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class LandingViewController: UIViewController {

    class func instantiateFromStoryboard() -> LandingViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LandingViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.HideNavigationBar()
    }

    @IBAction func homeAction(_ sender : UIButton ) {
        
        let controller = SideMenuRootViewController.instantiateFromStoryboard()
        controller.leftViewPresentationStyle = .scaleFromBig
        controller.leftViewWidth = 290.0
        AppDelegate.getInstatnce().window?.rootViewController = controller
        
    }

}

