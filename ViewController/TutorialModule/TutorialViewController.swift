//
//  ViewController.swift
//  QuizUp
//
//  Created by Admin on 27/06/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit


extension TutorialViewController:ViewPagerDataSource {
    
    class func instantiateFromStoryboard() -> TutorialViewController {
        let storyboard = UIStoryboard(name: "Tutorial", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! TutorialViewController
    }
    
    func numberOfItems(viewPager:ViewPager) -> Int {
        return 4;
    }
    
    func viewAtIndex(viewPager:ViewPager, index:Int, view:UIView?) -> UIView {
        
        var newView = view;
        var label:UILabel?
        var secondLabel:UILabel?
        var backgroundImg: UIImageView?
        var skipButton: UIButton?
        
        if(newView == nil){
            
            newView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height:  self.view.frame.height))
            newView!.backgroundColor = UIColor.clear
            
            backgroundImg = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            backgroundImg!.tag = 2
            
            label = UILabel(frame: newView!.bounds)
            label!.textColor = UIColor(red:0.00, green:0.45, blue:1.00, alpha:1.0)
            label!.numberOfLines = 0
            label!.tag = 0
            label!.autoresizingMask =  [.flexibleWidth, .flexibleHeight]
            label!.textAlignment = .center
            label!.frame.origin = CGPoint(x:0, y:0)
            let labelCustomFont = UIFont(name: "Avenir-Next-Regular", size: 30.0);
            label!.font = labelCustomFont
//            label!.font = UIFont.boldSystemFont(ofSize: 25.0) //label!.font.withSize(30)
            
            secondLabel = UILabel(frame: newView!.bounds)
            secondLabel!.textColor = UIColor.black
            secondLabel!.numberOfLines = 0
            secondLabel!.tag = 1
            secondLabel!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            secondLabel!.textAlignment = .center
            secondLabel!.frame.origin = CGPoint(x:0, y:30)
            let secondLabelCustomFont = UIFont(name: "Avenir-Next-Regular", size: 14.0);
            secondLabel!.font = secondLabelCustomFont
            
            
            skipButton = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
            skipButton!.frame.origin = CGPoint(x: self.view.center.x-55, y: self.view.frame.height-100)
            skipButton?.backgroundColor = UIColor(red:0.00, green:0.45, blue:1.00, alpha:1.0)
            skipButton!.tintColor = UIColor(red:0.00, green:0.45, blue:1.00, alpha:1.0)
            let skipBtnCustomFont = UIFont(name: "Avenir-Next-Medium", size: 25.0);
            skipButton?.titleLabel?.font = skipBtnCustomFont
            skipButton!.setTitle("Done", for: .normal)
            skipButton!.setTitleColor(UIColor.white, for: .normal)
            skipButton!.addTarget(self, action: #selector(skipButtonAction), for: .touchUpInside)
            skipButton!.layer.cornerRadius = 5;
            skipButton!.tag = 3
            
            newView?.addSubview(backgroundImg!)
            newView?.addSubview(label!)
            newView?.addSubview(secondLabel!)
            newView?.addSubview(skipButton!)
            
            
        } else {
            label = newView?.viewWithTag(0) as? UILabel
            secondLabel = newView?.viewWithTag(1) as? UILabel
            backgroundImg = newView?.viewWithTag(2) as? UIImageView
            skipButton = newView?.viewWithTag(3) as? UIButton
//            let heightContraints = NSLayoutConstraint(item: skipButton!, attribute:
//                .height, relatedBy: .equal, toItem: self.view,
//                         attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0,
//                         constant: 40)
//
//            let bottomContraints = NSLayoutConstraint(item: skipButton!, attribute:
//                .bottom, relatedBy: .equal, toItem: self.view,
//                         attribute: NSLayoutAttribute.bottom, multiplier: 1.0,
//                         constant: -60)
//
//            let leftContraints = NSLayoutConstraint(item: skipButton!, attribute:
//                .leadingMargin, relatedBy: .equal, toItem: self.view,
//                                attribute: .leadingMargin, multiplier: 1.0,
//                                constant: 0)
//
//            let rightContraints = NSLayoutConstraint(item: skipButton!, attribute:
//                .trailingMargin, relatedBy: .equal, toItem: self.view,
//                                 attribute: .trailingMargin, multiplier: 1.0,
//                                 constant: 0)
//            self.view.addConstraint(NSLayoutConstraint(item: skipButton!, attribute:
//                .height, relatedBy: .equal, toItem: self.view,
//                         attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0,
//                         constant: 40))
//            self.view.addConstraint(NSLayoutConstraint(item: skipButton!, attribute:
//                .bottom, relatedBy: .equal, toItem: self.view,
//                         attribute: NSLayoutAttribute.bottom, multiplier: 1.0,
//                         constant: -60))
//            self.view.addConstraint(NSLayoutConstraint(item: skipButton!, attribute:
//                .leading, relatedBy: .equal, toItem: self.view,
//                                attribute: .leading, multiplier: 1.0,
//                                constant: 0))
//            self.view.addConstraint(NSLayoutConstraint(item: skipButton!, attribute:
//                .trailing, relatedBy: .equal, toItem: self.view,
//                                 attribute: .trailing, multiplier: 1.0,
//                                 constant: 0))
            
//           NSLayoutConstraint.activate([heightContraints,rightContraints,leftContraints,bottomContraints])
//            self.view.addConstraints([bottomContraints, leftContraints, rightContraints, heightContraints])
        }
        
        if index == 0 {
            let attributedText = NSMutableAttributedString(string: "Tutorial Screen", attributes: [NSAttributedString.Key.underlineStyle : true])
//            label?.text = "WELCOME TO\nSCORECARTS"
            label?.attributedText = attributedText
            
            secondLabel?.text = "How to find an item You’re looking for"
            
            skipButton?.isHidden = true
//            self.backgroudImage.image = UIImage(named: "A")
//            backgroundImg?.image = UIImage(named: "A")
        } else if index == 1 {
//            label?.frame.origin = CGPoint(x: 0 , y: 150)
//            secondLabel!.frame.origin = CGPoint(x:0, y:200)
            let attributedText = NSMutableAttributedString(string: "Tutorial Screen", attributes: [NSAttributedString.Key.underlineStyle : true])
//            label?.text = "SHOP AND EARN"
            label?.attributedText = attributedText
            
            secondLabel?.text = "How to meet with Seller and Buyer"
            
            skipButton?.isHidden = true
//            self.backgroudImage.image = UIImage(named: "B")
//            backgroundImg?.image = UIImage(named: "B")
        } else if index == 2 {
//            label?.frame.origin = CGPoint(x: 0 , y: 150)
//            secondLabel!.frame.origin = CGPoint(x:0, y:200)
            let attributedText = NSMutableAttributedString(string: "Tutorial Screen", attributes: [NSAttributedString.Key.underlineStyle : true])
//            label?.text = "SHARE INFORMATION"
            label?.attributedText = attributedText
            
            secondLabel?.text = "Items will be deleted if in appropriate things get Posetd. Your item will also deleted"
            skipButton?.isHidden = true
            
//            self.backgroudImage.image = UIImage(named: "C")
//            backgroundImg?.image = UIImage(named: "C")
        }
        else {
            //            label?.frame.origin = CGPoint(x: 0 , y: 150)
            //            secondLabel!.frame.origin = CGPoint(x:0, y:200)
            let attributedText = NSMutableAttributedString(string: "Tutorial Screen", attributes: [NSAttributedString.Key.underlineStyle : true])
            //            label?.text = "SHARE INFORMATION"
            label?.attributedText = attributedText
            
            secondLabel?.text = "How to Post quality Product Images to help Sell your Stuff"
            skipButton?.isHidden = false
            
            //            self.backgroudImage.image = UIImage(named: "C")
//            backgroundImg?.image = UIImage(named: "C")
        }
        
        return newView!
    }
}

func didSelectedItem(index: Int) {
    print("select index \(index)")
}

class TutorialViewController: UIViewController {
    
    @IBOutlet weak var viewPager:ViewPager!
    @IBOutlet weak var backgroudImage:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewPager.dataSource = self
        self.viewPager.pageControl.isUserInteractionEnabled = false

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func skipAction(_ sender:UIButton) {
        
        let controller = SideMenuRootViewController.instantiateFromStoryboard()
        controller.leftViewPresentationStyle = .scaleFromBig
        controller.leftViewWidth = 290.0
        AppDelegate.getInstatnce().window?.rootViewController = controller
    }
    
    @objc func skipButtonAction(sender: UIButton) {
        let controller = SideMenuRootViewController.instantiateFromStoryboard()
        controller.leftViewPresentationStyle = .scaleFromBig
        controller.leftViewWidth = 290.0
        AppDelegate.getInstatnce().window?.rootViewController = controller
    }

}

