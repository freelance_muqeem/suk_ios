//
//  CategoriesCollectionViewCell.swift
//  Suk
//
//  Created by AQEEL AHMED on 14/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImgVw: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
}
