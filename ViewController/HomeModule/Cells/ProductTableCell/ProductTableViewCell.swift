//
//  ProductTableViewCell.swift
//  Suk
//
//  Created by AQEEL AHMED on 18/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {
    
    // IBOutlets
    @IBOutlet weak var sectionName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var productsArray = [Product]()
    
    // Variables
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // Set CollectionView Delegate and DataSource
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.collectionView.register(UINib(nibName: "ProductCell", bundle: .main), forCellWithReuseIdentifier: "ProductCollectionCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}


extension ProductTableViewCell : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth =  collectionView.bounds.size.width - 20
        let height = GISTUtility.convertToRatio(190)
        return CGSize(width: screenWidth/2.0 , height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//        let obj = self.MyCourseListArray![indexPath.row]
        
        let cell: ProductCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionCell", for: indexPath) as! ProductCollectionViewCell
        
        if self.productsArray[indexPath.row].totalSeen != nil {
            cell.noOfViewsLabel.text = String(self.productsArray[indexPath.row].totalSeen!)
        }
        
        if self.productsArray[indexPath.row].totalFavorite != nil {
            cell.noOfLikesLabel.text = String(self.productsArray[indexPath.row].totalFavorite!)
        }
        
//        cell.productImgVw.image = UIImage(named: "splash")
        if self.productsArray[indexPath.row].productImage != nil {
            cell.productImgVw.setImageFromUrl(urlStr: self.productsArray[indexPath.row].productImage!)
        }
        
        if self.productsArray[indexPath.row].isFavorite != nil {
            
            if self.productsArray[indexPath.row].isFavorite == 0 {
                cell.likeImgVw.image = UIImage(named: "heart_icon")
            }
            else {
                cell.likeImgVw.image = UIImage(named: "favorite_heart_button")
            }
            
        }
        
        cell.btnLikeProduct.addTarget(self, action: #selector(btnLikeProductAction(sender:)), for: .touchUpInside)
        
        cell.btnToProductDetails.addTarget(self, action: #selector(toProductDetails(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ProductSelected"), object: nil)
        
    }
    
    @objc func btnLikeProductAction(sender: UIButton){
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.collectionView)
        let indexPath = self.collectionView.indexPathForItem(at: buttonPosition)
        if indexPath != nil {
            let cell:ProductCollectionViewCell = self.collectionView.cellForItem(at: indexPath!) as! ProductCollectionViewCell
            
            if self.productsArray[indexPath!.row].isFavorite == 0 {
                self.productsArray[indexPath!.row].isFavorite = 1
                cell.likeImgVw.image = UIImage(named: "favorite_heart_button")
            }
            else {
                self.productsArray[indexPath!.row].isFavorite = 0
                cell.likeImgVw.image = UIImage(named: "heart_icon")
            }
            
        }

    }
    
    @objc func toProductDetails(sender: UIButton){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ProductSelected"), object: nil)
    }
}
