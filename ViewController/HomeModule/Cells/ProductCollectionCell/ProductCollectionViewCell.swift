//
//  ProductCollectionViewCell.swift
//  Suk
//
//  Created by AQEEL AHMED on 14/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    // Header View
    @IBOutlet weak var productImgVw: UIImageView!
    @IBOutlet weak var likeImgVw: UIImageView!
    
    @IBOutlet weak var btnLikeProduct: UIButton!
    @IBOutlet weak var btnToProductDetails: UIButton!
    
    
    // Bottom View
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var noOfLikesLabel: UILabel!
    @IBOutlet weak var noOfViewsLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setUpCollectionCellAppearance()
    }
    
    func setUpCollectionCellAppearance() {
//        self.layer.borderColor = UIColor(red:0.10, green:0.24, blue:0.31, alpha:1.0).cgColor
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1.0
        
        self.layer.masksToBounds = false;
        //    _self.layer.borderWidth = 1.0;
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.4;
        self.layer.shadowOffset = CGSize(width: 0, height: 0.5);
    }
    
}

