//
//  HomeViewController.swift
//  Suk
//
//  Created by Abdul Muqeem on 09/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension HomeViewController : AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class HomeViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    // IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    // Variables
//    let categoryNamesArray = ["CLOTHES","SERVICES","ELECTRONICS","JOB","HOMES","VEHICLES"]
//    let cetegoryImgArray = ["cloth_icon","services_icon","computer_icon","job_icon","home_icon","vehicle_icon"]
    var isLogout:Bool = false
    
    var categoriesArray:[Categories]? = [Categories]()
    var productsDict:Dictionary = [String:[Product]]()
    var allProductCategories = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add search Icon to Search TextField
        self.addSearchIconToSearchTextField()
        
        // Alert
        self.alertView.delegate = self
        self.alertView.delegateAction = self
        self.alertView.isHidden = true
        
        // Set up Nav Bar
        self.setAttributedNavTitle()
        self.addBarButtonItem()
        
        // Set collectionView delegate and dataSource
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        // Set tableView delegate and dataSource
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        // Register xib for collection cell
        self.collectionView.register(UINib(nibName: "CategoriesCell", bundle: .main), forCellWithReuseIdentifier: "CategoriesCollectionCell")
        
        // Register xib for Table cell
        self.tableView.register(UINib(nibName: "ProductTableCell", bundle: .main), forCellReuseIdentifier: "ProductTableCell")
        
//        if Singleton.sharedInstance.CurrentUser != nil  {
//            if let name = Singleton.sharedInstance.CurrentUser!.fullName {
//                print("Name: \(name)")
//            }
//            
//        }
        
        if self.isLogout == true {
            
            DispatchQueue.main.async {
                
                self.alertView.btnOkAction.setTitle("Logout", for: .normal)
                self.alertView.btnCancel.setTitle("Cancel", for: .normal)
                
                self.alertView.alertShow(image: FAILURE_IMAGE , title: "Alert", msg: "Are you sure you want to Logout ?", id: 1)
                self.alertView.isHidden = false
                
            }
        }
        
        if isLogout != true {
            // Get All Categories to user
            self.GetAllCategories()
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ProductSelected(_:)), name: NSNotification.Name(rawValue: "ProductSelected"), object: nil)
    }
    
    @objc func ProductSelected(_ notification:Notification) {
        // Do something now
        self.alertView.alertShow(image: SUCCESS_IMAGE, title: "Alert", msg: "Will implement it in future" , id: 0)
        self.alertView.isHidden = false
    }
    
    @IBAction func tabBarAction(_ sender: UIButton) {
        self.alertView.alertShow(image: SUCCESS_IMAGE, title: "Alert", msg: "Will implement it in future" , id: 0)
        self.alertView.isHidden = false
    }
    
    
}

// MARK:- CollectionView DataSource
extension HomeViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoriesArray!.count;
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let screenWidth =  collectionView.bounds.size.width - 20
//        let height = GISTUtility.convertToRatio(200)
//        return CGSize(width: screenWidth/2.0 , height: height)
//
//    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CategoriesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCollectionCell", for: indexPath) as! CategoriesCollectionViewCell
        
        // Set up Cell
        if self.categoriesArray?[indexPath.row] != nil {
            if self.categoriesArray?[indexPath.row].categoryName != nil {
                cell.categoryLabel.text = self.categoriesArray?[indexPath.row].categoryName
            }
            
            if self.categoriesArray?[indexPath.row].categoryImage != nil {
                let imgUrl = (self.categoriesArray?[indexPath.row].categoryImage)!
                cell.categoryImgVw.setImageFromUrl(urlStr: imgUrl)
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

// MARK:- TableView DataSource
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allProductCategories.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return GISTUtility.convertToRatio(440)
        }
        else {
            return GISTUtility.convertToRatio(230)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ProductTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProductTableCell", for: indexPath) as! ProductTableViewCell
        if indexPath.row == 0 {
            collectionView.isScrollEnabled = false
        }
        else {
            collectionView.isScrollEnabled = true
        }
        cell.productsArray = self.productsDict[self.allProductCategories[indexPath.row]]!
        cell.sectionName.text = self.allProductCategories[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
        }
        
    }
    
}


// MARK:- Navigation Bar Set up
extension HomeViewController {
    
    func addBarButtonItem() {
//        let profileBtn = UIButton(type: .custom)
//        profileBtn.setImage(UIImage(named: "user_placeholder_icon"), for: .normal)
//        profileBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//        profileBtn.addTarget(self, action: #selector(self.navigateToProfileScreen), for: .touchUpInside)
//        let item1 = UIBarButtonItem(customView: profileBtn)
        
        let menuBtn = UIButton(type: .custom)
        menuBtn.setImage(UIImage(named: "menu_icon"), for: .normal)
        menuBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        menuBtn.addTarget(self, action: #selector(self.showMenu), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: menuBtn)
        
//        self.navigationItem.setRightBarButton(item1, animated: true)
        self.navigationItem.setLeftBarButton(item2, animated: true)
        //        self.navigationItem.setRightBarButtonItems([item1,item2], animated: true)
    }
    
    func setAttributedNavTitle() {
//        let navLabel = UILabel()
//        navLabel.attributedText = self.appendIconAndTextForLabel(imageName: "location_placeholder_icon", textToAppend: "City,State")
        let navTxt = self.textFieldWithIconAndText(imageName: "location_placeholder_icon", textToAppend: "City,State")
        self.navigationItem.titleView = navTxt
    }
    
//    @objc func navigateToProfileScreen() {
//        let navigationController = sideMenuController?.rootViewController as! UINavigationController
//        let vc = MyProfileViewController.instantiateFromStoryboard()
//        navigationController.setViewControllers([vc],animated: false)
//        self.hideLeftViewAnimated(self)
//    }
    
    @objc func showMenu() {
        self.showLeftViewAnimated(self)
    }
}

// MARK:- Helper Functions
extension HomeViewController {
    func appendIconAndTextForLabel(imageName: String, textToAppend: String) -> NSMutableAttributedString {
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: imageName)
        let attachmentString = NSAttributedString(attachment: attachment)
        let attributedTextString = NSMutableAttributedString(string: textToAppend)
        attributedTextString.append(attachmentString)
        return attributedTextString
        //        self.mobileLabel.textAlignment = .center;
        //        self.mobileLabel.attributedText = completeText;
    }
    
    func textFieldWithIconAndText(imageName: String, textToAppend: String) -> UITextField {
        
        let textField = UITextField()
        textField.text = " "+textToAppend
        let imageView = UIImageView(image: UIImage(named: imageName))
        textField.leftViewMode = .always
        textField.leftView = imageView
        
        
        textField.isEnabled = false
        
        return textField
    }
    
    func addSearchIconToSearchTextField() {
        let imageView = UIImageView(image: UIImage(named: "icon_search"))
        self.searchTextField.leftViewMode = .always
        self.searchTextField.leftView = imageView
    }
    
}

//MARK:- Service Calls
extension HomeViewController {
    
    func GetAllCategories() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
//        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        UserServices.CategoriesList(param:[:] , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
//            print("Response: \(response!)")
            
            
            let categoriesResponseArray = response?["Result"].arrayValue
            for resultObj in categoriesResponseArray! {
                let obj = Categories(json: resultObj)
                self.categoriesArray?.append(obj)
            }
            
            if self.categoriesArray!.count > 0 {
                self.collectionView.reloadData()
            }
            
            self.GetAllProducts()
            
        })
        
    }
    
    
    func GetAllProducts() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        //        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        UserServices.ProductsList(param:[:] , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print("Response: \(response!)")
            
            
            let productsList = response?["Result"].dictionaryValue
//            print("Response: \(productsList!)")
            let allKeys = productsList?.keys
//            print("allKeys: \(allKeys!)")
            
            for key in allKeys! {
                let oneCategoryProductArray = productsList![key]!.arrayValue
                var oneCategoryProducts:[Product]? = [Product]()
                for obj in oneCategoryProductArray {
                    let product = Product(json: obj)
                    oneCategoryProducts?.append(product)
                }
                self.productsDict[key] = oneCategoryProducts
                self.allProductCategories.append(key)
            }
            
            let index:Int = self.allProductCategories.firstIndex(of: "Recommended")!
            self.allProductCategories.swapAt(index, 0)
//            print("allProductCategories: \(String(describing: self.allProductCategories))")
//            print("self.productsDict: \(self.productsDict)")
            
            if self.allProductCategories.count > 0 {
                self.tableView.reloadData()
            }
            
        })
        
    }
}

extension HomeViewController : AlertViewDelegateAction {
    
    func okButtonAction() {
        
        UserDefaults.standard.removeObject(forKey: User_data_userDefault)
        UserDefaults.standard.synchronize()
        Singleton.sharedInstance.CurrentUser = nil
        
        let nav = RootViewController.instantiateFromStoryboard()
        AppDelegate.getInstatnce().window?.rootViewController = nav
        let VC = SignInViewController.instantiateFromStoryboard()
        nav.pushViewController(VC, animated: true)
        
        
        
    }
    
    func cancelAction() {
        self.alertView.isHidden = true
        self.GetAllCategories()
    }
    
}
