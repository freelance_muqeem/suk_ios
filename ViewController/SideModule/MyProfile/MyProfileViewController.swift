//
//  MyProfileViewController.swift
//  Suk
//
//  Created by AQEEL AHMED on 21/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import ALCameraViewController

extension MyProfileViewController : AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class MyProfileViewController: UIViewController {

    class func instantiateFromStoryboard() -> MyProfileViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MyProfileViewController
    }
    
    // IBOutlets Header View
    @IBOutlet weak var profilePicImgVw: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    // IBOutlets --> Main View
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    var isInformationUpdated:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set up Navigation Bar
        self.addBarButtonItem()
        
        // By Default on this view isInformationUpdated flag will be false
        self.isInformationUpdated = false
        
        // By Default all the textfields would be disabled
        self.txtName.isEnabled = false
        self.txtEmail.isEnabled = false
        self.txtPhoneNumber.isEnabled = false
        self.txtPassword.isEnabled = false
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.initData()
        
    }
    
    @IBAction func editNameAction(_ sender: UIButton) {
        self.txtName.isEnabled = true
        self.isInformationUpdated = true
    }
    
    @IBAction func editEmailAction(_ sender: UIButton) {
        self.txtEmail.isEnabled = true
        self.isInformationUpdated = true
    }
    
    @IBAction func changePhoneNumberAction(_ sender: UIButton) {
        self.txtPhoneNumber.isEnabled = true
        self.isInformationUpdated = true
    }
    
    @IBAction func changePasswordAction(_ sender: UIButton) {
        self.txtPassword.isEnabled = true
        self.isInformationUpdated = true
    }
    
    
    @IBAction func updateAction(_ sender: UIButton) {
        
        if isInformationUpdated == true {
            self.view.endEditing(true)
            
            guard let name = self.txtName.text, AppHelper.isValidText(testStr: name) else {
                self.showBanner(title: "Error", subTitle: "Please enter valid Name" , style: .danger)
                return
            }
            
            guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
                self.showBanner(title: "Error", subTitle: "Please enter valid email" , style: .danger)
                return
            }
            
            guard let phoneNumber = self.txtPhoneNumber.text, AppHelper.isValidPhone(testStr: phoneNumber) else {
                self.showBanner(title: "Error", subTitle: "Please enter valid Phone Number" , style: .danger)
                return
            }
            
            guard let password = self.txtPassword.text, AppHelper.isValidPassword(testStr: password) else {
                self.showBanner(title: "Error", subTitle: "Please enter Existing or New Password" , style: .danger)
                return
            }
            
            self.startLoading(message: "")
            
            if !AppHelper.isConnectedToInternet() {
                self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
                self.stopLoading()
                return
            }
            
            // "email":email
            let id = Singleton.sharedInstance.CurrentUser!.id!
            let params:[String:Any] = ["user_id":id, "full_name":name, "phone":phoneNumber, "password":password, "city":"", "state":""]
            print("Parameter: \(params)")
            
            let profilePicture = self.profilePicImgVw.image!
            
            UserServices.UpdateProfile(param: params, image: profilePicture, completionHandler: {(status, response, error) in
                
                if !status {
                    if error != nil {
                        let error = String(describing: (error as AnyObject).localizedDescription)
                        print("Error: \(error)")
                        self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                        self.alertView.isHidden = false
                        self.stopLoading()
                        return
                    }
                    let msg = response?["Message"].stringValue
                    print("Message: \(String(describing: msg))")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                
                self.stopLoading()
                print("Response: \(response!)")
                
                let userResultObj = User(object:(response?["Result"])!)
                UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
                Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
                
                self.alertView.alertShow(image: SUCCESS_IMAGE, title: "Congratulations", msg: "Your account has been updated successfully", id: 0)
                self.alertView.isHidden = false
                self.textAlert = "Update Profile"
                
            })
            
        }
        else {
            self.showBanner(title: "Error", subTitle: "Please updaet Information before Updating" , style: .info)
            return
        }
        
        
    }
    
    
}

// MARK:- Navigation Bar Set up
extension MyProfileViewController {
    
    func addBarButtonItem() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.setImage(UIImage(named: "menu_icon"), for: .normal)
        menuBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        menuBtn.addTarget(self, action: #selector(self.showMenu), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: menuBtn)
        
        self.navigationItem.setLeftBarButton(item2, animated: true)
        //        self.navigationItem.setRightBarButtonItems([item1,item2], animated: true)
    }
    
    @objc func showMenu() {
        self.showLeftViewAnimated(self)
    }
}

//MARK:- CameraViewController
extension MyProfileViewController : UINavigationControllerDelegate , UIImagePickerControllerDelegate {
    
    @IBAction func profilePicAction(_ sender: UIButton) {
        self.isInformationUpdated = true
//        self.openCamera()
        self.PresentChooseImageSourceActionSheet()
    }
    
    func openCamera() {
        
        let cameraViewController = CameraViewController { [weak self] image, asset in
            if let pickedImage = image {
                self?.profilePicImgVw.image = pickedImage
            }
            
            self?.dismiss(animated: true, completion: nil)
            
        }
        
        present(cameraViewController, animated: true, completion: nil)
    }
}

//MARK:- Helper Functions
extension MyProfileViewController {
    func initData() {
        if let fullName = Singleton.sharedInstance.CurrentUser!.fullName {
            self.txtName.text = fullName
            self.lblUserName.text = fullName
        }
        
        if let phone = Singleton.sharedInstance.CurrentUser!.phone {
            self.txtPhoneNumber.text = phone
        }
        
        if let email = Singleton.sharedInstance.CurrentUser!.email {
            self.txtEmail.text = email
        }
        
        self.txtPassword.placeholder = "********"
        
        if let profilePictureUrl = Singleton.sharedInstance.CurrentUser?.profileImage {
            self.profilePicImgVw.setImageFromUrl(urlStr: profilePictureUrl)
        }
    }
    
    
    func PresentChooseImageSourceActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet);
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let cameraActionHandler = { (action: UIAlertAction!) in
            self.chooseImageFromCamera();
        }
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: cameraActionHandler)
        let photoLibraryActionHandler = { (action: UIAlertAction!) in
            self.chooseImageFromPhotoLibrary();
        }
        let photoLibraryAction = UIAlertAction(title: "Photos", style: .default, handler: photoLibraryActionHandler);
        cancelAction.setValue(UIColor.black, forKey: "titleTextColor");
        cameraAction.setValue(UIColor.black, forKey: "titleTextColor");
        photoLibraryAction.setValue(UIColor.black, forKey: "titleTextColor");
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(photoLibraryAction)
        actionSheet.addAction(cancelAction)
        
        self.present(actionSheet, animated: true, completion: nil);
    }
}

// MARK:- Image Picker Controller Delegte Methods
extension MyProfileViewController {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let chosenImage: UIImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage;
        self.profilePicImgVw.image = chosenImage
//        let imageData: Data = chosenImage.jpegData(compressionQuality: 0.8)!
        
        picker.dismiss(animated: true, completion:{
            // For now nothing
        });
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil);
    }
    
    func chooseImageFromCamera() {
        let picker  = UIImagePickerController()
        picker.delegate = self;
        picker.allowsEditing = true;
        picker.sourceType = .camera;
        
        self.present(picker, animated: true, completion: nil);
        
    }
    
    func chooseImageFromPhotoLibrary () {
        let picker  = UIImagePickerController()
        picker.delegate = self;
        picker.allowsEditing = true;
        picker.sourceType = .photoLibrary;
        
        self.present(picker, animated: true, completion: nil);
        
    }
    
}
