//
//  FAQAndSupportViewController.swift
//  Suk
//
//  Created by AQEEL AHMED on 27/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension FAQAndSupportViewController : AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class FAQAndSupportViewController: UIViewController {

    class func instantiateFromStoryboard() -> FAQAndSupportViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! FAQAndSupportViewController
    }
    
    @IBOutlet weak var txtFAQAndSupport: UITextView!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Alert
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        // Set up Nav Bar
        self.addBarButtonItem()
        
        // Get Privacy policy from API
        self.GetFAQAndSupport()
    }

}

// MARK:- Navigation Bar Set up
extension FAQAndSupportViewController {
    
    func addBarButtonItem() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.setImage(UIImage(named: "menu_icon"), for: .normal)
        menuBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        menuBtn.addTarget(self, action: #selector(self.showMenu), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: menuBtn)
        
        self.navigationItem.setLeftBarButton(item2, animated: true)
        //        self.navigationItem.setRightBarButtonItems([item1,item2], animated: true)
    }
    
    @objc func showMenu() {
        self.showLeftViewAnimated(self)
    }
}

//MARK:- Service Calls
extension FAQAndSupportViewController {
    
    func GetFAQAndSupport() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        //        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        UserServices.Support(param:[:] , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print("Response: \(response!)")
            
            if let text = response?["Result"]["content"].string {
                self.txtFAQAndSupport.text = text
            }
            
        })
        
    }
}
