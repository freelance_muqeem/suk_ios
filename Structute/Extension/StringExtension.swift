//
//  StringExtension.swift
//  HireMile
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation

/*
 extension UITableView {
 
 public func reloadData(_ completion: @escaping ()->()) {
 UIView.animate(withDuration: 0, animations: {
 self.reloadData()
 }, completion:{ _ in
 completion()
 })
 }
 
 func scroll(to: scrollsTo, animated: Bool) {
 DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
 let numberOfSections = self.numberOfSections
 let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
 switch to{
 case .top:
 if numberOfRows > 0 {
 let indexPath = IndexPath(row: 0, section: 0)
 self.scrollToRow(at: indexPath, at: .top, animated: animated)
 }
 break
 case .bottom:
 if numberOfRows > 0 {
 let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
 self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
 }
 break
 }
 }
 }
 
 enum scrollsTo {
 case top,bottom
 }
 }
 */

extension Collection {
    
    subscript(optional i: Index) -> Iterator.Element? {
        return self.indices.contains(i) ? self[i] : nil
    }
    
}

extension UserDefaults {
    
    func object<T: Codable>(_ type: T.Type, with key: String, usingDecoder decoder: JSONDecoder = JSONDecoder()) -> T? {
        guard let data = self.value(forKey: key) as? Data else { return nil }
        return try? decoder.decode(type.self, from: data)
    }
    
    func set<T: Codable>(object: T, forKey key: String, usingEncoder encoder: JSONEncoder = JSONEncoder()) {
        let data = try? encoder.encode(object)
        self.set(data, forKey: key)
    }
}

extension Int {
    
    private static var numberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        
        return numberFormatter
    }()
    
    var decimal: String {
        return Int.numberFormatter.string(from: NSNumber(value: self)) ?? ""
    }
}

extension Data {
    
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension Double {
    static func / (left: Double, right: Int) -> Double {
        return left / Double(right)
    }
}

extension String {
    
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                substring(with: substringFrom..<substringTo)
            }
        }
    }
    
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
    
    func toInt() -> Int? {
        return NumberFormatter().number(from: self)?.intValue
    }
    
    func isEmptyOrWhitespace() -> Bool {
        if(self.isEmpty) {
            return true
        }
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces) == ""
    }
    
    func removeNumbers() -> String {
        return components(separatedBy: CharacterSet.decimalDigits).joined(separator: "")
    }
    
    func removeLetters() -> String {
        return components(separatedBy: CharacterSet.letters).joined(separator: "")
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    func removingLeadingSpaces() -> String {
        guard let index = firstIndex(where: { !CharacterSet(charactersIn: String($0)).isSubset(of: .whitespaces) }) else {
            return self
        }
        return String(self[index...])
    }
    
    func GetDate(format: String) -> String {
        
        var serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverDateFormatter2: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverDateFormatter3: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverDateFormatter4: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverDateFormatter5: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "dd-MM-yyyy HH:mm:ss a"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 5) as TimeZone
            return result
        }()
        
        if let localDate = serverDateFormatter.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
        
        
        if let localDate = serverDateFormatter2.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
        
        if let localDate = serverDateFormatter3.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
        if let localDate = serverDateFormatter4.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
        
        if let localDate = serverDateFormatter5.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
            
            
        else {
            serverDateFormatter = {
                let result = DateFormatter()
                result.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
                return result
            }()
            
            let localDate = serverDateFormatter.date(from: self)!
            
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
    }
    
    
    
    func GetTime() -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "dd/mm/yyyy hh:mm:ss a"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverTime = self
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localTimeFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "hh:mm a"
            return result
        }()
        
        return localTimeFormatter.string(from: localTime)
    }
    
    func GetTime(toFormat: String) -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverTime = self
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localTimeFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = toFormat
            return result
        }()
        
        
        return localTimeFormatter.string(from: localTime)
    }
    
    func GetTime(fromFormat: String) -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = fromFormat
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverTime = self
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localTimeFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "hh:mm a"
            return result
        }()
        
        
        return localTimeFormatter.string(from: localTime)
    }
    
    func GetTime(fromFormat: String, toFormat: String) -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = fromFormat
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverTime = self
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localTimeFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = toFormat
            return result
        }()
        
        
        return localTimeFormatter.string(from: localTime)
    }
}




