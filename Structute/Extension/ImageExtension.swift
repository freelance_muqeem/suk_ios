//
//  ImageExtension.swift
//  HireMile
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIView {

    /**
     Fade in a view with a duration
     
     - parameter duration: custom animation duration
     */
    func fadeIn(withDuration duration: TimeInterval = 0.5) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    /**
     Fade out a view with a duration
     
     - parameter duration: custom animation duration
     */
    func fadeOut(withDuration duration: TimeInterval = 0.5) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
    
}


extension UIImageView {

    func setImageFromUrl(urlStr:String) {

        let tempStr = urlStr.replacingOccurrences(of: "\\", with: "/")
        let tempStr1 = tempStr.replacingOccurrences(of: " ", with: "%20")
        let url = URL(string: tempStr1)!
        print(url)
        
        var kingf = self.kf
        kingf.indicatorType = .activity
        kingf.setImage(with: url)
        
       // kingf.setImage(with: url, placeholder: nil, options: [.transition(.fade(1)), progressBlock: nil, completionHandler: nil)
    }

}

//extension UIImageView {
//
//    func setImageFromUrl url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
//        contentMode = mode
//        URLSession.shared.dataTask(with: url) { data, response, error in
//            guard
//                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
//                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
//                let data = data, error == nil,
//                let image = UIImage(data: data)
//                else { return }
//            DispatchQueue.main.async() {
//                self.image = image
//            }
//            }.resume()
//    }
//
//    func setImageFromUrl link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
//        guard let url = URL(string: link) else { return }
//        setImageFromUrl(urlStr: url, contentMode: mode)
//    }
//}

extension UIButton {
    
    func setImageURL(urlStr:String) {
        
        let url = URL(string: urlStr)!
        print(url)
        self.kf.setImage(with: url, for: .normal)

    }
}

extension UIFont {
    
    func withTraits(traits:UIFontDescriptor.SymbolicTraits...) -> UIFont {
        let descriptor = self.fontDescriptor
            .withSymbolicTraits(UIFontDescriptor.SymbolicTraits(traits))
        return UIFont(descriptor: descriptor!, size: 0)
    }
    
    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }
}
