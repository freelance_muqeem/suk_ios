
//
//  AppDelegate.swift
//  ParkingOnTheGo
//
//  Created by Minhasoft on 4/24/18.
//  Copyright © 2018 Minhasoft. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import NotificationBannerSwift

extension Comparable {
    func clamped(to limits: ClosedRange<Self>) -> Self {
        return min(max(self, limits.lowerBound), limits.upperBound)
    }
}
extension Dictionary where Value: Equatable {
    func allKeys(forValue val: Value) -> [Key] {
        return self.filter { $1 == val }.map { $0.0 }
    }
}

extension Strideable where Stride: SignedInteger {
    func clamped(to limits: CountableClosedRange<Self>) -> Self {
        return min(max(self, limits.lowerBound), limits.upperBound)
    }
}

extension UIApplication
{
    var statusBarView: UIView?
    {
        if responds(to: Selector(("statusBar")))
        {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

extension UIViewController : NVActivityIndicatorViewable {
    
    //MARK:- Loader 
    
    func startLoading(message:String){
        let size = CGSize(width: 80, height:80)
        startAnimating(size, message: message , type:.ballTrianglePath)
        
    }
    
    func stopLoading(){
        stopAnimating()
    }
    
    //MARK:- Banner
    func showBanner(title:String , subTitle:String , style:BannerStyle ) {
        
        let banner = NotificationBanner(title: title, subtitle: subTitle, style: style)
        banner.show()
        
    }
    
    //MARK:- Alert Controller
    
    // Alert Popup  Ok
    func Alert(title : String,message : String) {
        
        let alert = UIAlertController(title: title as String, message: message as String, preferredStyle: UIAlertController.Style.alert)
        
        let alertAction = UIAlertAction(title: NSLocalizedString("OK", comment: "") , style: UIAlertAction.Style.default) { (action) -> Void in
            // do something after completation
        }
        
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // Alert Popup  Ok Action
    func ShowAlertAction(title : String,message : String, OkActionHandler:@escaping (_ message:Any?)->Void){
        
        let alert = UIAlertController(title: title as String, message: message as String, preferredStyle: UIAlertController.Style.alert)
        
        let alertAction = UIAlertAction(title: NSLocalizedString("Ok", comment: "") , style: UIAlertAction.Style.default) { (action) -> Void in
            // do something after completation
            OkActionHandler("pressed ok")
        }
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)

    }

    // Alert Popup  Ok Cancel Action
    func ShowAlert(title : String,message : String, OkActionHandler:@escaping (_ message:Any?)->Void){
        
        let alert = UIAlertController(title: title as String, message: message as String, preferredStyle: UIAlertController.Style.alert)
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        let doneAction = UIAlertAction(title: NSLocalizedString("Yes", comment: "") , style: UIAlertAction.Style.default) { (action) -> Void in
            // do something after completation
            OkActionHandler("pressed ok")
        }
        
        alert.addAction(cancelAction)
        alert.addAction(doneAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func PlaceNavImage(image: UIImage) {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
       // let logo = UIImage(named: "logo.png")
        let imageView = UIImageView(image:image)
        self.navigationItem.titleView = imageView
        
    }
    
    func PlaceNavImageBackButton(image: UIImage, selector: Selector , titleImage: UIImage) {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        // let logo = UIImage(named: "logo.png")
        let imageView = UIImageView(image:titleImage)
        self.navigationItem.titleView = imageView
        
        var btnLeft: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setImage(image, for: .normal)
        btnLeft?.addTarget(self, action: selector, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    //MARK:- Left Button Code
    
    func PlaceLeftButton(image: UIImage, selector: Selector) {
        
        var btnLeft: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setImage(image, for: .normal)
        btnLeft?.addTarget(self, action: selector, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    //MARK:- Left Button Title Code
    
    func PlaceLeftButton(selectorForLeftText : Selector , leftTitle : String)  {
        
        var btnLeft: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setTitle(leftTitle, for: .normal)
        btnLeft?.addTarget(self, action: selectorForLeftText, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    //MARK:- Right Button Title Code
    
    func PlaceRightButton(selectorForRightText : Selector , rightTitle : String)  {
        
        var btnRight: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnRight = UIButton(type: .custom)
        btnRight?.setTitle(rightTitle, for: .normal)
        btnRight?.addTarget(self, action: selectorForRightText, for: .touchUpInside)
        btnRight?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnRight!)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    
    //MARK:- Right Button Code
    
    func PlaceRightButton(image: UIImage, selector: Selector) {
        
        var btnRight: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnRight = UIButton(type: .custom)
        btnRight?.setImage(image, for: .normal)
        btnRight?.addTarget(self, action: selector, for: .touchUpInside)
        btnRight?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnRight!)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
    }
    
    
    
    //MARK:-  Left Button & right Button Code
    
    func PlaceNavigationButtons(selectorForLeftText : Selector, leftTitle:String ,  selectorForRightText : Selector , rightTitle:String) {
        
        var btnLeft: UIButton?
        var btnRight: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setTitle(leftTitle, for: .normal)
        btnLeft?.addTarget(self, action: selectorForLeftText, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
        btnRight = UIButton(type: .custom)
        btnRight?.setTitle(rightTitle, for: .normal)
        btnRight?.addTarget(self, action: selectorForRightText, for: .touchUpInside)
        btnRight?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnRight!)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    func PlaceNavigationButtons(selectorForLeft : Selector, leftImage:UIImage ,  selectorForRight : Selector , rightImage:UIImage) {
        
        var btnLeft: UIButton?
        var btnRight: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setImage(leftImage, for: .normal)
        btnLeft?.addTarget(self, action: selectorForLeft, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.leftBarButtonItems = [ leftBarButton ]
        
        btnRight = UIButton(type: .custom)
        btnRight?.setImage(rightImage, for: .normal)
        btnRight?.addTarget(self, action: selectorForRight, for: .touchUpInside)
        btnRight?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnRight!)
        self.navigationItem.rightBarButtonItems = [ rightBarButton ]
        
    }
    
    
    //MARK:-  Left Buttons with right button title Code
    
    func PlaceLeftButtons(selectorForLeft : Selector, leftImage : UIImage , selectorForRightText : Selector , rightTitle : String) {
        
        var btnLeft: UIButton?
        var btnRightText: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setImage(leftImage, for: .normal)
        btnLeft?.addTarget(self, action: selectorForLeft, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.leftBarButtonItems = [ leftBarButton ]
        
        btnRightText = UIButton(type: .custom)
        btnRightText?.setTitle(rightTitle, for: .normal)
        btnRightText?.addTarget(self, action: selectorForRightText, for: .touchUpInside)
        btnRightText?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let rightTextBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnRightText!)
        self.navigationItem.rightBarButtonItems = [ rightTextBarButton ]
        
    }
    
    func PlaceNavigationBackButtons(selectorForLeftLogo : Selector, leftLogoImage : UIImage , selectorForLeftText : Selector , leftTitle : String) {
        
        var btnLogo: UIButton?
        var btnLeftText: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLogo = UIButton(type: .custom)
        btnLogo?.setImage(leftLogoImage, for: .normal)
        btnLogo?.addTarget(self, action: selectorForLeftLogo, for: .touchUpInside)
        btnLogo?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        let leftBtnLogo: UIBarButtonItem = UIBarButtonItem(customView: btnLogo!)
        
        btnLeftText = UIButton(type: .custom)
        btnLeftText?.setTitle(leftTitle, for: .normal)
        btnLeftText?.addTarget(self, action: selectorForLeftText, for: .touchUpInside)
        btnLeftText?.frame = CGRect(x: 0, y: 0 , width: 200, height: 25)
        let leftBtnText: UIBarButtonItem = UIBarButtonItem(customView: btnLeftText!)
        
        self.navigationItem.leftBarButtonItems = [ leftBtnLogo , leftBtnText ]
    }
    
    func PlaceNavigationHomeButtons(selectorForLeftLogo : Selector, leftLogoImage : UIImage ,selectorForRightSearch : Selector, rightSearchImage : UIImage , selectorForRightCart : Selector, rightCartImage : UIImage ) {
        
        var btnLogo: UIButton?
        
        var btnSearch: UIButton?
        var btnCart: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLogo = UIButton(type: .custom)
        btnLogo?.setImage(leftLogoImage, for: .normal)
        btnLogo?.addTarget(self, action: selectorForLeftLogo, for: .touchUpInside)
        btnLogo?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        let leftBtnLogo: UIBarButtonItem = UIBarButtonItem(customView: btnLogo!)
        
        self.navigationItem.leftBarButtonItems = [ leftBtnLogo  ]

        btnSearch = UIButton(type: .custom)
        btnSearch?.setImage(rightSearchImage, for: .normal)
        btnSearch?.addTarget(self, action: selectorForRightSearch, for: .touchUpInside)
        btnSearch?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        let RightBtnSearch: UIBarButtonItem = UIBarButtonItem(customView: btnSearch!)
        
        btnCart = UIButton(type: .custom)
        btnCart?.setImage(rightCartImage, for: .normal)
        btnCart?.addTarget(self, action: selectorForRightCart, for: .touchUpInside)
        btnCart?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        let RightBtnCart: UIBarButtonItem = UIBarButtonItem(customView: btnCart!)
        
        self.navigationItem.rightBarButtonItems = [ RightBtnSearch , RightBtnCart ]
    }
    
}




