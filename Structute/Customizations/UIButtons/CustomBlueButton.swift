//
//  CustomBlueButton.swift
//  Suk
//
//  Created by AQEEL AHMED on 21/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class CustomBlueButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setUpButton() {
        self.backgroundColor = UIColor(red:0.00, green:0.45, blue:1.00, alpha:1.0)
        
        self.layer.borderColor = UIColor(red:0.00, green:0.45, blue:1.00, alpha:1.0).cgColor
        self.layer.borderWidth = 1.0
        
        self.layer.shadowColor = UIColor(red:0.00, green:0.45, blue:1.00, alpha:1.0).cgColor
        self.layer.shadowOpacity = 0.4;
        self.layer.shadowOffset = CGSize(width: 0, height: 0.5);
        
        self.layer.masksToBounds = false;
        
        // Set up font of UIButton
        self.titleLabel?.font = UIFont(name: "Avenir-Next-Regular ", size: 18)
    }
}
