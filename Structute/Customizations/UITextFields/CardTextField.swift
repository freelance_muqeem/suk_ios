//
//  CardTextField.swift
//  Suk
//
//  Created by AQEEL AHMED on 20/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class CardTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpCardTextField()
    }

    // Function to make the UITextFields look like CardView TextField
    func setUpCardTextField() {
        
        self.backgroundColor = UIColor.white;
        
        self.layer.borderColor = UIColor.white.cgColor;
        self.layer.borderWidth = 1.0;
        
        self.layer.shadowColor = UIColor.lightGray.cgColor;
        self.layer.shadowOpacity = 0.4;
        self.layer.shadowOffset = CGSize(width: 0, height: 0.5);
        
        self.layer.cornerRadius = 6.0;
        
        self.layer.masksToBounds = false;
        
    }
}
