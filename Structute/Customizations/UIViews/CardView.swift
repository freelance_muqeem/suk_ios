//
//  CardView.swift
//  Suk
//
//  Created by AQEEL AHMED on 20/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class CardView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpCardView()
    }

    // Function to make the UIView look like CardView
    func setUpCardView() {
        
        self.backgroundColor = UIColor.white;
        
        self.layer.borderColor = UIColor.white.cgColor;
        self.layer.borderWidth = 1.0;
        
        self.layer.shadowColor = UIColor.lightGray.cgColor;
        self.layer.shadowOpacity = 0.4;
        self.layer.shadowOffset = CGSize(width: 0, height: 0.5);
        
        self.layer.cornerRadius = 12.0;
        
        self.layer.masksToBounds = false;
        
    }
    
}
