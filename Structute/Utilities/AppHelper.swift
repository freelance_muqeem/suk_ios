//
//  AppHelper.swift
//  HireMile
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire

class AppHelper: NSObject {
    
    static func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    static func isValidEmail(testStr:String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: testStr)
    }
    
    static func isValidPassword(testStr:String) -> Bool {
        if testStr.count < 6 {
            return false
        }
        return true
    }
    
    // atleast 1 Capital letter , 1 Number & 1 Special Character Or Minimum lenght is 6 Digit
//    static public func isValidPassword(testStr:String) -> Bool {
//        let passwordRegex = //"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{6,12}"
//         "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,12}$"
//        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: testStr)
//    }
    
    static func isValidText (testStr:String) -> Bool {
        if testStr.count <= 0 {
            return false
        }
        return true
    }
    
    static func isValidAge(testStr:String) -> Bool {
        if testStr == "0" {
            return false
        }
        if Int.init(testStr)! > 150 {
            return false
        }
        if testStr.count > 3 {
            return false
        }
        return true
    }
    
    static func isValidPhone(testStr:String) -> Bool {
        if testStr.count < 9 {
            return false
        } else if testStr.count > 15 {
            return false
        }
        return true
    }
}
