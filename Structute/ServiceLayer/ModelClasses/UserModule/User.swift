//
//  Body.swift
//
//  Created by Abdul Muqeem on 25/12/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class User: NSObject , NSCoding {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let state = "state"
        static let email = "email"
        static let fullName = "full_name"
        static let profileImage = "profile_image"
        static let isVerify = "is_verify"
        static let deviceType = "device_type"
        static let status = "status"
        static let roleId = "role_id"
        static let id = "id"
        static let deviceToken = "device_token"
        static let notificationStatus = "notification_status"
        static let phone = "phone"
        static let profilePicture = "profile_picture"
        static let token = "_token"
    }
    
    // MARK: Properties
    public var state: String?
    public var email: String?
    public var fullName: String?
    public var profileImage: String?
    public var isVerify: Int?
    public var deviceType: String?
    public var status: Int?
    public var roleId: Int?
    public var id: Int?
    public var deviceToken: String?
    public var notificationStatus: Int?
    public var phone: String?
    public var profilePicture: String?
    public var token: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        state = json[SerializationKeys.state].string
        email = json[SerializationKeys.email].string
        fullName = json[SerializationKeys.fullName].string
        profileImage = json[SerializationKeys.profileImage].string
        isVerify = json[SerializationKeys.isVerify].int
        deviceType = json[SerializationKeys.deviceType].string
        status = json[SerializationKeys.status].int
        roleId = json[SerializationKeys.roleId].int
        id = json[SerializationKeys.id].int
        deviceToken = json[SerializationKeys.deviceToken].string
        notificationStatus = json[SerializationKeys.notificationStatus].int
        phone = json[SerializationKeys.phone].string
        profilePicture = json[SerializationKeys.profilePicture].string
        token = json[SerializationKeys.token].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = state { dictionary[SerializationKeys.state] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = fullName { dictionary[SerializationKeys.fullName] = value }
        if let value = profileImage { dictionary[SerializationKeys.profileImage] = value }
        if let value = isVerify { dictionary[SerializationKeys.isVerify] = value }
        if let value = deviceType { dictionary[SerializationKeys.deviceType] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = roleId { dictionary[SerializationKeys.roleId] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = deviceToken { dictionary[SerializationKeys.deviceToken] = value }
        if let value = notificationStatus { dictionary[SerializationKeys.notificationStatus] = value }
        if let value = phone { dictionary[SerializationKeys.phone] = value }
        if let value = profilePicture { dictionary[SerializationKeys.profilePicture] = value }
        if let value = token { dictionary[SerializationKeys.token] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.state = aDecoder.decodeObject(forKey: SerializationKeys.state) as? String
        self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
        self.fullName = aDecoder.decodeObject(forKey: SerializationKeys.fullName) as? String
        self.profileImage = aDecoder.decodeObject(forKey: SerializationKeys.profileImage) as? String
        self.isVerify = aDecoder.decodeObject(forKey: SerializationKeys.isVerify) as? Int
        self.deviceType = aDecoder.decodeObject(forKey: SerializationKeys.deviceType) as? String
        self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? Int
        self.roleId = aDecoder.decodeObject(forKey: SerializationKeys.roleId) as? Int
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.deviceToken = aDecoder.decodeObject(forKey: SerializationKeys.deviceToken) as? String
        self.notificationStatus = aDecoder.decodeObject(forKey: SerializationKeys.notificationStatus) as? Int
        self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
        self.profilePicture = aDecoder.decodeObject(forKey: SerializationKeys.profilePicture) as? String
        self.token = aDecoder.decodeObject(forKey: SerializationKeys.token) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(state, forKey: SerializationKeys.state)
        aCoder.encode(email, forKey: SerializationKeys.email)
        aCoder.encode(fullName, forKey: SerializationKeys.fullName)
        aCoder.encode(profileImage, forKey: SerializationKeys.profileImage)
        aCoder.encode(isVerify, forKey: SerializationKeys.isVerify)
        aCoder.encode(deviceType, forKey: SerializationKeys.deviceType)
        aCoder.encode(status, forKey: SerializationKeys.status)
        aCoder.encode(roleId, forKey: SerializationKeys.roleId)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(deviceToken, forKey: SerializationKeys.deviceToken)
        aCoder.encode(notificationStatus, forKey: SerializationKeys.notificationStatus)
        aCoder.encode(phone, forKey: SerializationKeys.phone)
        aCoder.encode(profilePicture, forKey: SerializationKeys.profilePicture)
        aCoder.encode(token, forKey: SerializationKeys.token)
    }
    
}
