//
//  Product.swift
//
//  Created by AQEEL AHMED on 01/08/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Product: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let totalFavorite = "total_favorite"
        static let id = "id"
        static let productDescription = "product_description"
        static let image = "image"
        static let totalSeen = "total_seen"
        static let productName = "product_name"
        static let isFavorite = "is_favorite"
        static let price = "price"
        static let productImage = "product_image"
    }
    
    // MARK: Properties
    public var totalFavorite: Int?
    public var id: Int?
    public var productDescription: String?
    public var image: String?
    public var totalSeen: Int?
    public var productName: String?
    public var isFavorite: Int?
    public var price: String?
    public var productImage: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        totalFavorite = json[SerializationKeys.totalFavorite].int
        id = json[SerializationKeys.id].int
        productDescription = json[SerializationKeys.productDescription].string
        image = json[SerializationKeys.image].string
        totalSeen = json[SerializationKeys.totalSeen].int
        productName = json[SerializationKeys.productName].string
        isFavorite = json[SerializationKeys.isFavorite].int
        price = json[SerializationKeys.price].string
        productImage = json[SerializationKeys.productImage].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = totalFavorite { dictionary[SerializationKeys.totalFavorite] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = productDescription { dictionary[SerializationKeys.productDescription] = value }
        if let value = image { dictionary[SerializationKeys.image] = value }
        if let value = totalSeen { dictionary[SerializationKeys.totalSeen] = value }
        if let value = productName { dictionary[SerializationKeys.productName] = value }
        if let value = isFavorite { dictionary[SerializationKeys.isFavorite] = value }
        if let value = price { dictionary[SerializationKeys.price] = value }
        if let value = productImage { dictionary[SerializationKeys.productImage] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.totalFavorite = aDecoder.decodeObject(forKey: SerializationKeys.totalFavorite) as? Int
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.productDescription = aDecoder.decodeObject(forKey: SerializationKeys.productDescription) as? String
        self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
        self.totalSeen = aDecoder.decodeObject(forKey: SerializationKeys.totalSeen) as? Int
        self.productName = aDecoder.decodeObject(forKey: SerializationKeys.productName) as? String
        self.isFavorite = aDecoder.decodeObject(forKey: SerializationKeys.isFavorite) as? Int
        self.price = aDecoder.decodeObject(forKey: SerializationKeys.price) as? String
        self.productImage = aDecoder.decodeObject(forKey: SerializationKeys.productImage) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(totalFavorite, forKey: SerializationKeys.totalFavorite)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(productDescription, forKey: SerializationKeys.productDescription)
        aCoder.encode(image, forKey: SerializationKeys.image)
        aCoder.encode(totalSeen, forKey: SerializationKeys.totalSeen)
        aCoder.encode(productName, forKey: SerializationKeys.productName)
        aCoder.encode(isFavorite, forKey: SerializationKeys.isFavorite)
        aCoder.encode(price, forKey: SerializationKeys.price)
        aCoder.encode(productImage, forKey: SerializationKeys.productImage)
    }
    
}
