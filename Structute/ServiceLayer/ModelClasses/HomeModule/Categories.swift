//
//  Categories.swift
//
//  Created by AQEEL AHMED on 27/07/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Categories: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let parentId = "parent_id"
    static let id = "id"
    static let image = "image"
    static let categoryImage = "category_image"
    static let categoryName = "category_name"
  }

  // MARK: Properties
  public var status: String?
  public var parentId: Int?
  public var id: Int?
  public var image: String?
  public var categoryImage: String?
  public var categoryName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    status = json[SerializationKeys.status].string
    parentId = json[SerializationKeys.parentId].int
    id = json[SerializationKeys.id].int
    image = json[SerializationKeys.image].string
    categoryImage = json[SerializationKeys.categoryImage].string
    categoryName = json[SerializationKeys.categoryName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = parentId { dictionary[SerializationKeys.parentId] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = categoryImage { dictionary[SerializationKeys.categoryImage] = value }
    if let value = categoryName { dictionary[SerializationKeys.categoryName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.parentId = aDecoder.decodeObject(forKey: SerializationKeys.parentId) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
    self.categoryImage = aDecoder.decodeObject(forKey: SerializationKeys.categoryImage) as? String
    self.categoryName = aDecoder.decodeObject(forKey: SerializationKeys.categoryName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(parentId, forKey: SerializationKeys.parentId)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(categoryImage, forKey: SerializationKeys.categoryImage)
    aCoder.encode(categoryName, forKey: SerializationKeys.categoryName)
  }

}
