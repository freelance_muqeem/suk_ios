//
//  UserServices.swift
//  Edu Lights
//
//  Created by Abdul Muqeem on 24/10/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserServices {
    
    // MARK:- POST APIs
    
    // MARK: login
    static func Login(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.login, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK: register
    static func Register(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.register, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK: Forgot Password
    static func ForgotPassword(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.forgotPassword, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK: Update Profile
    static func UpdateProfile(param:[String:Any], image:UIImage, completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.UploadImage(fromSavedUrl: ServiceApiEndPoints.updateProfile, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK:- GET API's
    
    // MARK: CategoriesList
    static func CategoriesList(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.categoriesList, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK: Products List
    static func ProductsList(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.productsList, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK: Terms And Conditions
    static func TermsAndConditions(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.termsConditions, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK: Privacy Policy
    static func PrivacyPolicy(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.privacyPolicy, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK: Support
    static func Support(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.support, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
}
