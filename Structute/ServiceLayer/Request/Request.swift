//
//  Request.swift
//  Tutorial
//
//  Created by Abdul Muqeem on 15/06/2017.
//  Copyright © 2017 Abdul Muqeem. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Request: NSObject {
    
    static func GetRequestWithHeader(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        print("parameters : \(parameters)")
        
        let token = Singleton.sharedInstance.CurrentUser!.token!
        let headers:[String:String] = ["Authorization" : token]
        print("Header: \(headers)")
        
        Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            var redirectedRequest = request
            
            if let originalRequest = task.originalRequest,
                let headers = originalRequest.allHTTPHeaderFields,
                let authorizationHeaderValue = headers["Authorization"]
            {
                var mutableRequest = request
                mutableRequest.setValue(authorizationHeaderValue, forHTTPHeaderField: "Authorization")
                redirectedRequest = mutableRequest
            }
            
            return redirectedRequest
        }
        
        Alamofire.request(url, method: .get, parameters: parameters , headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func GetRequest(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        Alamofire.request(url, method: .get, parameters: parameters , headers: [:]).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func PostRequestWithHeader(fromSavedUrl url: String , parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        let token = Singleton.sharedInstance.CurrentUser!.token!
        let headers:[String:String] = ["token" : token]
        print("Token: \(headers)")
        
        Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            var redirectedRequest = request
            
            if let originalRequest = task.originalRequest,
                let headers = originalRequest.allHTTPHeaderFields,
                let authorizationHeaderValue = headers["token"]
            {
                var mutableRequest = request
                mutableRequest.setValue(authorizationHeaderValue, forHTTPHeaderField: "token")
                redirectedRequest = mutableRequest
            }
            
            return redirectedRequest
        }
        
        // Alamofire.request(url, method: .post, parameters: parameters , encoding: JSONEncoding.prettyPrinted , headers: headers).responseJSON{ (response:DataResponse<Any>) in
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseJSON{ (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func PostRequest(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        //   Alamofire.request(url, method: .post, parameters: parameters , encoding: JSONEncoding.prettyPrinted , headers: [:]).responseJSON{ (response:DataResponse<Any>) in
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: [:]).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func PostRequestwithQuery(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        let token = Singleton.sharedInstance.CurrentUser!.token!
        let headers:[String:String] = ["Authorization" : token]
        print("Header: \(headers)")
        
        Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            var redirectedRequest = request
            
            if let originalRequest = task.originalRequest,
                let headers = originalRequest.allHTTPHeaderFields,
                let authorizationHeaderValue = headers["Authorization"]
            {
                var mutableRequest = request
                mutableRequest.setValue(authorizationHeaderValue, forHTTPHeaderField: "Authorization")
                redirectedRequest = mutableRequest
            }
            
            return redirectedRequest
        }
        
        
        Alamofire.request(url, method: .post, parameters: parameters , encoding: URLEncoding.queryString, headers: [:]).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func PutRequestWithHeader(fromSavedUrl url: String , parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        let token = Singleton.sharedInstance.CurrentUser!.token!
        let headers:[String:String] = ["Authorization" : token]
        print("Header: \(headers)")
        
        Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            var redirectedRequest = request
            
            if let originalRequest = task.originalRequest,
                let headers = originalRequest.allHTTPHeaderFields,
                let authorizationHeaderValue = headers["Authorization"]
            {
                var mutableRequest = request
                mutableRequest.setValue(authorizationHeaderValue, forHTTPHeaderField: "Authorization")
                redirectedRequest = mutableRequest
            }
            
            return redirectedRequest
        }
        
        Alamofire.request(url, method: .put , parameters: parameters ,encoding: JSONEncoding.prettyPrinted, headers: headers).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func DeleteRequestWithHeader(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        let token = Singleton.sharedInstance.CurrentUser!.token!
        let headers:[String:String] = ["Authorization" : token]
        print("Header: \(headers)")
        
        Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            var redirectedRequest = request
            
            if let originalRequest = task.originalRequest,
                let headers = originalRequest.allHTTPHeaderFields,
                let authorizationHeaderValue = headers["Authorization"]
            {
                var mutableRequest = request
                mutableRequest.setValue(authorizationHeaderValue, forHTTPHeaderField: "Authorization")
                redirectedRequest = mutableRequest
            }
            
            return redirectedRequest
        }
        
        Alamofire.request(url, method: .delete, parameters: parameters,encoding: JSONEncoding.prettyPrinted , headers: headers).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func DeleteRequest(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        Alamofire.request(url, method: .delete, parameters: parameters, headers: [:]).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func downloadFile(url: String, callback: ((String?) -> Void)?) {
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let file = directoryURL.appendingPathComponent("myFileName.pdf", isDirectory: false)
            return (file, [.createIntermediateDirectories, .removePreviousFile])
        }
        
        Alamofire.download(url, to:destination)
            .downloadProgress { (progress) in
                print((String)(progress.fractionCompleted))
            }
            .responseData { (data) in
                
                //Get the local docs directory and append your local filename.
                var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last as NSURL?
                
                docURL = docURL?.appendingPathComponent( "myFileName.pdf") as NSURL?
                
                //Lastly, write your file to the disk.
                do {
                    try data.value?.write(to: docURL! as URL)//.writeToURL(docURL!, atomically: true)
                }
                catch {
                    
                }
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                let path = paths[0]
                
                let pathToFile = (path as NSString).appendingPathComponent("myFileName.pdf")
                
                callback!(pathToFile)
                if FileManager.default.fileExists(atPath: pathToFile){
                    print("file Exist")
                    return
                }
                //Alert.showAlert(title: "Alert", message: "Your download has been completed")
                print(data)
        }
        
    }
    
    func getSaveFileUrl(fileName: String) -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL;
    }
    
    static func UploadMultipleImages(fromSavedUrl url: String, parameters: [String: Any] ,images: [UIImage], callback: ((JSON?, Error?) -> Void)?) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (index, img) in images.enumerated() {
                let imageData =  img.jpegData(compressionQuality: 0.75)
                multipartFormData.append(imageData!, withName: "education_documents[\(index)]", fileName: "file.jpg", mimeType: "image/jpg")
            }
            for (key, value) in parameters {
                multipartFormData.append(("\(value)" as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                print("Key::\(key) -> Value::\(value)")
            }
        }, to: url)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("progress::\(progress)")
                })
                //self.uploadRequest = upload.responseData(completionHandler: { response in
                upload.responseData(completionHandler: { response in
                    
                    if response.result.isSuccess {
                        print("SUCCESS")
                        do {
                            let jsonData = try? JSON.init(data: response.data!)
                            callback?(jsonData, nil)
                        }
                    }
                    else {
                        print("FAILURE")
                        print("\(String(describing: (response.error?.localizedDescription)!))")
                        callback?(nil, response.error)
                    }
                })
                
            case .failure(let encodingError):
                print("Fail:: \(encodingError.localizedDescription)")
                callback?(nil, encodingError)
                
                break
            }
        }
    }
    
    static func UploadImage(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                
                if let data = value as? Data {
                    multipartFormData.append(data, withName: "profile_picture", fileName: "file.jpeg", mimeType: "image/jpeg")
                    //  multipartFormData.append(data, withName: key, fileName: "file.jpg", mimeType: "image/jpg")
                    //  multipartFormData.append(data, withName: "", fileName: "filename.jpg", mimeType: "image/jpg")
                } else  {
                    multipartFormData.append(("\(value)" as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
            }
        }, to: url, method: .post,headers: [:])
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("progress::\(progress)")
                })
                upload.responseData(completionHandler: { response in
                    
                    if response.result.isSuccess {
                        if let value: Any = response.result.value as AnyObject? {
                            let response = JSON(value)
                            callback?(response, nil)
                        }
                    }
                    else {
                        print("FAILURE")
                        print("\(String(describing: (response.error?.localizedDescription)!))")
                        callback?(nil, response.error)
                    }
                })
                
            case .failure(let encodingError):
                print("Fail:: \(encodingError.localizedDescription)")
                callback?(nil, encodingError)
                
                break
            }
        }
    }
    
    static func UploadParkingSpot(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        let token = Singleton.sharedInstance.CurrentUser!.token!
        let headers = ["Authorization" : "Bearer "+token+""]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                
                if let data = value as? Data {
                    multipartFormData.append(data, withName: "spot_image", fileName: "filename.jpg", mimeType: "image/jpg")
                } else  {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
            }
        }, to: url, method: .post,headers: headers)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("progress::\(progress)")
                })
                upload.responseData(completionHandler: { response in
                    
                    if response.result.isSuccess {
                        if let value: Any = response.result.value as AnyObject? {
                            let response = JSON(value)
                            callback?(response, nil)
                        }
                    }
                    else {
                        print("FAILURE")
                        print("\(String(describing: (response.error?.localizedDescription)!))")
                        callback?(nil, response.error)
                    }
                })
                
            case .failure(let encodingError):
                print("Fail:: \(encodingError.localizedDescription)")
                callback?(nil, encodingError)
                
                break
            }
        }
    }
    
    // Update My Profile
    static func UploadSingleMultipleImages(fromSavedUrl url: String, parameters: [String: Any] ,images: [UIImage] , image: UIImage , callback: ((JSON?, Error?) -> Void)?) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (index, img) in images.enumerated() {
                let imageData =  img.jpegData(compressionQuality: 0.75)
                let image =  image.jpegData(compressionQuality: 0.75)
                multipartFormData.append(imageData!, withName: "education_documents[\(index)]", fileName: "file.jpg", mimeType: "image/jpg")
                multipartFormData.append(image!, withName: "spot_image", fileName: "file.jpg", mimeType: "image/jpg")
            }
            for (key, value) in parameters {
                multipartFormData.append(("\(value)" as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                print("Key::\(key) -> Value::\(value)")
            }
        }, to: url)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("progress::\(progress)")
                })
                //self.uploadRequest = upload.responseData(completionHandler: { response in
                upload.responseData(completionHandler: { response in
                    
                    if response.result.isSuccess {
                        print("SUCCESS")
                        do {
                            let jsonData = try? JSON.init(data: response.data!)
                            callback?(jsonData, nil)
                        }
                    }
                    else {
                        print("FAILURE")
                        print("\(String(describing: (response.error?.localizedDescription)!))")
                        callback?(nil, response.error)
                    }
                })
                
            case .failure(let encodingError):
                print("Fail:: \(encodingError.localizedDescription)")
                callback?(nil, encodingError)
                
                break
            }
        }
    }

    
}





