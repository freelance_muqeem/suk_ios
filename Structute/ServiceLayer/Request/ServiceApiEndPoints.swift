//
//  ServiceApiEndPoints.swift
//  EduLights
//
//  Created by Abdul Muqeem on 22/03/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import Foundation

class ServiceApiEndPoints: NSObject  {

    static let baseURL = "http://blindcircle.com/suk/api/"
    
    // User Module
    static let login = baseURL + "login"
    static let register = baseURL + "register"
    static let forgotPassword = baseURL + "forgotpassword"
    
    // Side Module
    static let updateProfile = baseURL + "profile/update"
    static let termsConditions = baseURL + "termsConditions"
    static let privacyPolicy = baseURL + "privacyPolicy"
    static let support = baseURL + "support"
    
    // Home Module
    static let categoriesList = baseURL + "categories"
    static let productsList = baseURL + "category/products"
    
}
