//
//  MyCourseCollectionCell.swift
//  EduLights
//
//  Created by Abdul Muqeem on 12/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class MyCourseCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imgLogo:UIImageView!
    @IBOutlet weak var lblCourseName:UILabel!
    @IBOutlet weak var lblInstituteName:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
