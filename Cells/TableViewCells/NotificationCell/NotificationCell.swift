//
//  NotificationCell.swift
//  EduLights
//
//  Created by Abdul Muqeem on 18/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
