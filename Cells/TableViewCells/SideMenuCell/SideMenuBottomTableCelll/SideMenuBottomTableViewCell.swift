//
//  SideMenuBottomTableViewCell.swift
//  Suk
//
//  Created by AQEEL AHMED on 30/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class SideMenuBottomTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
