//
//  AppDelegate.swift
//  Suk
//
//  Created by Abdul Muqeem on 09/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import LGSideMenuController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // Override point for customization after application launch.
       // UIApplication.shared.statusBarView?.backgroundColor = UIColor(named: "ThemeColor")!
        
        IQKeyboardManager.shared.enable = true
        self.NavigateTOInitialViewController()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }

    func applicationDidBecomeActive(_ application: UIApplication) {

    }

    func applicationWillTerminate(_ application: UIApplication) {

    }

    static func getInstatnce() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func NavigateTOInitialViewController() {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        if UserManager.isUserLogin() {
            
            let controller = SideMenuRootViewController.instantiateFromStoryboard()
            controller.leftViewPresentationStyle = .scaleFromBig
            controller.leftViewWidth = 290.0
            self.window?.rootViewController = controller
            
        } else {
            
            let nav = RootViewController.instantiateFromStoryboard()
            self.window?.rootViewController = nav
            let vc = WelcomeViewController.instantiateFromStoryboard()
            nav.pushViewController(vc, animated: true)
            
        }
    }

}

